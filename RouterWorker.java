import org.json.simple.JSONObject;

public class RouterWorker implements Runnable {
	public String getSvcQueue(String svc){
		return "svcQueue"+svc;
	}
    @SuppressWarnings("unchecked")
	public void run() {
    	communicateWithMQ mqHandler = new communicateWithMQ();
    	communicateWithMQ mqHandler2 = new communicateWithMQ();
    	while(true){
    		JSONObject jsonReq = mqHandler.listenAndConsume("svcQueueLoadRouter");
    		if(jsonReq.containsKey("svcName")){
    			String svcName = (String)jsonReq.get("svcName");
    			mqHandler2.send(getSvcQueue(svcName), jsonReq, "svcQueueLoadRouterReply");
    			mqHandler.reply(mqHandler2.listenAndFilter());
    		}
    		else{
    			JSONObject json = new JSONObject();
    			json.put("response", "error: no such service");
    			mqHandler.reply(json);
    		}    		
    	}
    }
}