FROM openjdk
CMD mkdir -p /var/serverless
WORKDIR /var/serverless
CMD java -cp .:amqp-client-4.1.0.jar:json-simple-1.1.1.jar:json-20090211.jar:slf4j-api-1.7.21.jar:slf4j-simple-1.7.22.jar serviceListener
