import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

@SuppressWarnings("unchecked")
public class Monitoring extends DoLogging {
	HashMap<String, MachineStats> machineStats=new HashMap<>();
	HashMap<String, ServiceStats> serviceStats=new HashMap<>();
	public void updateStats(JSONObject jSon) {
		JSONObject tempJO=new JSONObject();
		try {
			MachineStats md=new MachineStats();
			JSONArray jsonArrayMachine=(JSONArray)jSon.get("stats");
			md.setMachine((String)jSon.get("machine"));
			md.setCpu((Double)((JSONObject)jsonArrayMachine.get(1)).get("cpu"));
			md.setRam((Double)((JSONObject)((JSONObject)jsonArrayMachine.get(1))).get("ram"));
			md.setProcess(((Long)((JSONObject)(JSONObject)jsonArrayMachine.get(1)).get("processes")).intValue());

			HashMap<String, Integer> mapOfSvcListForMchn = new HashMap<>();

			JSONArray JAOfSvcListForMchn = (JSONArray)((JSONObject)jsonArrayMachine.get(0)).get("instances");
			int len=JAOfSvcListForMchn.size();
			for (int i = 0; i < len; i++) {
				tempJO=(JSONObject)JAOfSvcListForMchn.get(i);
				Set<String> s =tempJO.keySet();
				String temp=s.iterator().next();
				mapOfSvcListForMchn.put(temp,Integer.parseInt(tempJO.get(temp).toString()));

			}
			md.setInstances(mapOfSvcListForMchn);
			machineStats.put((String)jSon.get("machine"),md);

			for (int i = 0; i < len	; i++) {
				tempJO=(JSONObject)JAOfSvcListForMchn.get(i);
				Set<String> s =tempJO.keySet();
				String temp=s.iterator().next();
				ServiceStats ss;
				if (serviceStats.containsKey(temp)) {
					ss=serviceStats.get(temp);
				}else {
					ss=new ServiceStats();
				}

				if (ss.getServiceName()=="") {
					ss.setServiceName(temp);
					serviceStats.put(temp, ss);
				}

				HashMap<String, Integer>tempHM;
				tempHM=serviceStats.get(temp).getServiceList();
				if (tempHM==null) {
					tempHM=new HashMap<>();
				}
				tempHM.put((String)jSon.get("machine"),Integer.parseInt(tempJO.get(temp).toString()));

				ss.setServiceList(tempHM);
				serviceStats.put(temp,ss);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	public void removeMachine(JSONObject jSon) {
		String machineName=(String)jSon.get("removeMachine");
		if(machineStats.containsKey(machineName)){
			HashMap<String, Integer> svcInstances=machineStats.get(machineName).getInstances();
			ArrayList<String>svcList=new ArrayList<>();
			for(Entry<String, Integer> e : svcInstances.entrySet()) {
				if(serviceStats.containsKey(e.getKey())) {
					ServiceStats ss=serviceStats.get(e.getKey());
					HashMap<String, Integer>tempHM=ss.getServiceList();
					if(tempHM.containsKey(machineName)) {
						tempHM.remove(machineName);
						if (tempHM.size()==0) {
							svcList.add(e.getKey());
						}else {
							ss.setServiceList(tempHM);						
							serviceStats.put(e.getKey(), ss);
						}
					}
				}
			}
			for(Iterator<String> it=svcList.iterator();it.hasNext();)
				serviceStats.remove(it.next());
			machineStats.remove(machineName);			
		}
	}
	public JSONObject getCompleteStats() {
		JSONObject jSon=new JSONObject();
		JSONArray jsonArray=new JSONArray();
		JSONObject tempJO;
		MachineStats tempMS;
		ServiceStats tempSS;
		for(Entry<String, MachineStats> e:machineStats.entrySet()) {
			tempJO=new JSONObject();
			tempMS=e.getValue();
			tempJO.put("machine",tempMS.getMachine() );
			tempJO.put("cpu", tempMS.getCpu());
			tempJO.put("ram", tempMS.getRam());
			tempJO.put("processes", tempMS.getProcess());
			jsonArray.add(tempJO);
		}
		jSon.put("machine", jsonArray);
		jsonArray = new JSONArray();
		tempJO = new JSONObject();
		for(Entry<String, ServiceStats> e : serviceStats.entrySet()){
			tempJO = new JSONObject();
			tempSS=e.getValue();
			int total=0;
			HashMap<String, Integer> tempHM=tempSS.getServiceList();
			for(Entry<String, Integer> e1 : tempHM.entrySet()) {
				total+=e1.getValue();
			}
			tempJO.put("name", e.getKey());
			tempJO.put("instances", total);
			jsonArray.add(tempJO);
			total=0;
		}
		jSon.put("svc", jsonArray);
		System.out.println(jSon.toString());
		return jSon;
	}
	public JSONObject svcList(String host){  //data to be sent to service LC Mgr
		JSONArray jsonArray =new JSONArray();
		JSONObject jSon =new JSONObject();
		if(machineStats.containsKey(host)){
			MachineStats ms=machineStats.get(host);
			HashMap<String, Integer> hm=ms.getInstances();
			for(Entry<String, Integer> e: hm.entrySet()) {
				JSONObject tmp =new JSONObject();
				tmp.put("name", e.getKey());
				tmp.put("instances", e.getValue().intValue());
				jsonArray.add(tmp);
			}
		}
		jSon.put("serviceList", jsonArray);
		System.out.println(jSon.toString());
		return jSon;
	}
	
	public JSONObject getAllMachineLevelStats(){
		JSONObject jSon=new JSONObject();
		JSONArray jsonArray =new JSONArray();
		JSONObject tempJO;
		JSONObject svcJO;
		JSONArray svcLstJA;
		MachineStats tempMS;
		for(Entry<String, MachineStats> e:machineStats.entrySet()) {
			tempJO=new JSONObject(); 
			tempMS=e.getValue();
			tempJO.put("machine",tempMS.getMachine() );
			tempJO.put("cpu", tempMS.getCpu());
			tempJO.put("ram", tempMS.getRam());
			tempJO.put("processes", tempMS.getProcess());
			svcLstJA=new JSONArray();
			for(Entry<String, Integer> e1:tempMS.getInstances().entrySet()) {
				svcJO=new JSONObject();
				svcJO.put(e1.getKey(), "name");
				svcJO.put("instances", e1.getValue());
				svcLstJA.add(svcJO);
			}
			tempJO.put("svc", svcLstJA);
			jsonArray.add(tempJO);
		}
		jSon.put("machineStats",jsonArray);
		System.out.println(jSon.toString());
		return jSon;
	}
	
	public JSONObject getAllSvcLevelStats() {
		JSONObject jSon=new JSONObject();
		JSONArray jsonArray =new JSONArray();
		JSONObject tempJO;
		JSONObject machineJO;
		JSONArray machineLstJA;
		ServiceStats tempSS;
		Integer sum = 0;
		for(Entry<String, ServiceStats> e:serviceStats.entrySet()) {
			tempJO=new JSONObject();  
			tempSS=e.getValue();
			tempJO.put("name",tempSS.getServiceName());
			machineLstJA=new JSONArray();
			for(Entry<String, Integer> e1:tempSS.getServiceList().entrySet()) {
				machineJO=new JSONObject();
				machineJO.put("name", e1.getKey());
				machineJO.put("instances", e1.getValue());
				machineLstJA.add(machineJO);
				sum+=e1.getValue();
			}
			tempJO.put("instances",sum);
			sum = 0;
			tempJO.put("machines", machineLstJA);
			jsonArray.add(tempJO);
		}
		jSon.put("svcStats",jsonArray);
		System.out.println(jSon.toString());
		return jSon;
	}
	
	public static void main(String[] args) {
		Monitoring monitoringObj = new Monitoring();
		JSONObject req = null;
		while(true){
			try {
				req = monitoringObj.listenAndConsume("svcQueueMonitoring");
				if(req.containsKey("stats"))	//Agent sent stats to Monitoring
					monitoringObj.updateStats(req);
				else if(req.containsKey("killMachineStats"))//ServiceLCMgr asked for all svc stats corresponding to a machine
					monitoringObj.reply(monitoringObj.svcList((String)req.get("killMachineStats")));
				else if(req.containsKey("removeMachine"))	//ServerLCMgr sent to Monitoring to clear stats
					monitoringObj.removeMachine(req);			
				else if(req.containsKey("giveMachineStats"))	//ServerLCMgr/any other asked for stats (all stats)
					monitoringObj.reply(monitoringObj.getCompleteStats());
				else if(req.containsKey("getAllMachineLevelStats"))	//LB asked for complete m/c level stats
					monitoringObj.reply(monitoringObj.getAllMachineLevelStats());
				else if(req.containsKey("getAllSvcLevelStats"))	//LB asked for complete svc level stats
					monitoringObj.reply(monitoringObj.getAllSvcLevelStats());
				else
					monitoringObj.doLogging(req,"Monitoring",System.getenv("HOSTNAME"),ManagementFactory.getRuntimeMXBean().getName().split("@")[0],"mainRequestListener","Wrong request type","ERROR");
				
			} catch (Exception e) {
				e.printStackTrace();
				monitoringObj.doLogging(req,"Monitoring",System.getenv("HOSTNAME"),ManagementFactory.getRuntimeMXBean().getName().split("@")[0],"mainRequestListener","Exception : "+e.getMessage(),"ERROR");
			}
		}
	}
	
	/*
	public void testSvcStats(HashMap<String, ServiceStats> hm) {
		for(Entry<String, ServiceStats> m:hm.entrySet()){  
			System.out.println(m.getKey());
			if (m.getValue() instanceof ServiceStats) {
				ServiceStats ss=(ServiceStats)m.getValue();
				HashMap<String, Double>tt=ss.getServiceList();
				for (Entry<String, Double> iterable_element : tt.entrySet()) {
					System.out.println("             "+iterable_element.getKey() +" "+ iterable_element.getValue());
				}
			}
		} 
	}


	public void testMachineStats(HashMap<String, MachineStats> hm) {
		for(Entry<String, MachineStats> m:hm.entrySet()){  
			System.out.println(m.getKey());
			if (m.getValue() instanceof MachineStats) {
				MachineStats mm=(MachineStats)m.getValue();
				System.out.println("           "+mm.getMachine());
				System.out.println("           "+mm.getCpu());
				System.out.println("           "+mm.getProcess());
				System.out.println("           "+mm.getRam());
				HashMap<String, Double>h=mm.getInstances();
				for(Entry<String, Double> hh : h.entrySet()) {
					System.out.println("           "+hh.getKey() + " "+hh.getValue());
				}
			}
		} 
	}
	public void testAllStats(JSONObject jSon){
		JSONArray jA=(JSONArray)jSon.get("machine");
		int len=jA.size();
		System.out.println("machine");
		for (int i = 0; i < len; i++) {
			System.out.println(jA.get(i).toString());
		}
		jA=(JSONArray)jSon.get("svc");
		len=jA.size();
		System.out.println("svc");
		for (int i = 0; i < len; i++) {
			System.out.println(jA.get(i).toString());
		}
	}



	public JSONObject createJson() {
		JSONObject js;
		String s="{\"machine\":\"trs21\",\"stats\":[{\"type\":\"svc\",\"instances\":[{\"svc1\":10},{\"svc2\":15}]},{\"type\":\"machine\",\"cpu\":89.2,\"ram\":1024.67,\"processes\":190}]}";
		try {
			js= (JSONObject) JSONValue.parse(s);;
			return js;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	public JSONObject createJson1() {
		JSONObject js;
		try {
			String s="{\"machine\":\"trs20\",\"stats\":[{\"type\":\"svc\",\"instances\":[{\"svc1\":25},{\"svc2\":20}]},{\"type\":\"machine\",\"cpu\":23.2,\"ram\":50244.67,\"processes\":3000}]}";
			js=(JSONObject)JSONValue.parse(s);
			return js;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	public HashMap<String, MachineStats> getMachineStats() {
		return machineStats;
	}
	public void setMachineStats(HashMap<String, MachineStats> machineStats) {
		this.machineStats = machineStats;
	}
	public HashMap<String, ServiceStats> getServiceStats() {
		return serviceStats;
	}
	public void setServiceStats(HashMap<String, ServiceStats> serviceStats) {
		this.serviceStats = serviceStats;
	}*/
} 
