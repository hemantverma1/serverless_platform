import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import com.rabbitmq.client.ConnectionFactory;                                                                                    
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.AlreadyClosedException;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.ShutdownListener;
import com.rabbitmq.client.ShutdownSignalException;
import com.rabbitmq.client.Consumer;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeoutException;


public class communicateWithMQ{
	final static String defaultExchange = "amq.direct"; 
	final static String nfsDir = "/var/serverless/";
	String correlationId;
	String replyMqName;
	String ip;
	int port;
	String user;
	String password;
	Connection connection;

	public communicateWithMQ(){
		Properties brokerProps = getBrokerDetails();
		ip = (String)brokerProps.get("ip");
		port = Integer.parseInt((String)brokerProps.get("port"));
		user = (String)brokerProps.get("user");
		password = (String)brokerProps.get("password");
		/*Get a ConnectionFactory object */
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(ip);
		factory.setPort(port);
		factory.setUsername(user);
		factory.setPassword(password);
		try{
			/* Create a TCP/IP connection */
			connection = factory.newConnection();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	public void destroy(){
		try {
			if(connection.isOpen())
				connection.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public int reply(JSONObject message){ 
		/*Used when a request has already been listened 
		through this object and then if you want to revert for that  request*/
		try{
			/* Create a channel over that TCP/IP connection */
			Channel channel = connection.createChannel();			
			String routingKey = replyMqName;
			AMQP.BasicProperties props = new AMQP.BasicProperties
					.Builder()
					.correlationId(correlationId)
					.build();
			channel.basicPublish(defaultExchange, routingKey, props, message.toString().getBytes("UTF-8"));
			//if(channel.isOpen()) channel.close();
		}
		catch(Exception e){
			e.printStackTrace();
			return 0;
		}
		return 1;

	}

	public int replyWithHeader(JSONObject message, Map<String, Object> headersMap){ 
		/*Used when a request has already been listened 
		through this object and then if you want to revert for that  request -- add the headers map in the MQ msg header*/
		try {
			/*Get a ConnectionFactory object */
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost(ip);
			factory.setPort(port);
			factory.setUsername(user);
			factory.setPassword(password);
			/* Create a connection */
			Connection connection = factory.newConnection();
			/* Create a channel over that TCP/IP connection */
			Channel channel = connection.createChannel();
			String routingKey = replyMqName;
			AMQP.BasicProperties props = new AMQP.BasicProperties
					.Builder()
					.correlationId(correlationId)
					.headers(headersMap)
					.build();
			channel.basicPublish(defaultExchange, routingKey, props, message.toString().getBytes("UTF-8"));
			//if(channel.isOpen()) channel.close();
			if(connection.isOpen()) connection.close();
		}
		catch(Exception e){
			e.printStackTrace();
			return 0;
		}
		return 1;

	}

	public int send(String mqName, JSONObject message, String replyMq){
		/*Used when you want to send a new msg to the specified MQ
		 *Side effect: correlation Id and replyMq name will be stored in 
		 * this object only
		 */
		try {
			/* Create a channel over that TCP/IP connection */
			Channel channel = connection.createChannel();			
			String routingKey = mqName;
			String replyQueueName = replyMq;
			/* get a unique correlation id */
			String corrId = UUID.randomUUID().toString();
			AMQP.BasicProperties props = new AMQP.BasicProperties
					.Builder()
					.correlationId(corrId)
					.replyTo(replyQueueName)
					.build();
			channel.basicPublish(defaultExchange, routingKey, props, message.toString().getBytes("UTF-8"));
			correlationId = corrId;
			replyMqName = replyMq;
			//if(channel.isOpen()) channel.close();
		}
		catch(Exception e){
			e.printStackTrace();
			return 0;
		}
		return 1;
	}

	public int sendWithHeader(String mqName, JSONObject message, String replyMq, Map<String, Object> headersMap){
		/*Used when you want to send a new msg to the specified MQ with header-fields set in the msg headers
		 *Side effect: correlation Id and replyMq name will be stored in 
		 * this object only
		 */
		try {
			/*Get a ConnectionFactory object */
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost(ip);
			factory.setPort(port);
			factory.setUsername(user);
			factory.setPassword(password);
			/* Create a connection */
			Connection connection = factory.newConnection();
			/* Create a channel over that TCP/IP connection */
			Channel channel = connection.createChannel();
			String routingKey = mqName;
			String replyQueueName = replyMq;
			/* get a unique correlation id */
			String corrId = UUID.randomUUID().toString();
			AMQP.BasicProperties props = new AMQP.BasicProperties
					.Builder()
					.correlationId(corrId)
					.replyTo(replyQueueName)
					.headers(headersMap)
					.build();
			channel.basicPublish(defaultExchange, routingKey, props, message.toString().getBytes("UTF-8"));
			correlationId = corrId;
			replyMqName = replyMq;
			//if(channel.isOpen()) channel.close();
			if(connection.isOpen()) connection.close();
		}
		catch(Exception e){
			e.printStackTrace();
			return 0;
		}
		return 1;
	}

	private static Properties getBrokerDetails(){
		Properties properties = new Properties();
		try{
			InputStream inputStream = new FileInputStream(nfsDir + "brokerDetails.cfg");
			properties.load(inputStream);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return properties;
	}

	public JSONObject listenAndFilter(){
		/*Used when you have already sent a msg with some correlationId and now
		 * want to listen for its reply against the same correlation id.
		 */
		JSONObject ret = null;
		String mqName = replyMqName;
		try{
			/* Create a channel over that TCP/IP connection */
			Channel channel = connection.createChannel();			
			BlockingQueue<JSONObject> response = new ArrayBlockingQueue<JSONObject>(1);
			Consumer consumer = new DefaultConsumer(channel) {
				@Override
				public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
					try{
						if (properties.getCorrelationId().equals(correlationId)) {
							JSONParser parser = new JSONParser();
							try {
								channel.basicAck(envelope.getDeliveryTag(), false);
								response.offer((JSONObject) parser.parse(new String(body, "UTF-8")));
							}
							catch(Exception e){
								e.printStackTrace();
							}
							Thread.currentThread().interrupt();
							return;
						}
						else if(channel.isOpen())
							channel.basicNack(envelope.getDeliveryTag(), false, true);						
					}
					catch(AlreadyClosedException e) {System.out.println("aya");e.printStackTrace();}
				}
			};

			channel.basicConsume(mqName, false, consumer);
			//System.out.println(response.size());
			ret = response.take();
			if(channel.isOpen()) channel.abort();
			//if(connection.isOpen()) connection.close();
			/*
			final BlockingQueue<JSONObject> response = new ArrayBlockingQueue<JSONObject>(1);
		    channel.basicConsume(mqName, true, new DefaultConsumer(channel) {
		      @Override
		      public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
		    	//System.out.println("Got msg with corr id: " + properties.getCorrelationId());
		        if (properties.getCorrelationId().equals(correlationId)) {
		        	JSONParser parser = new JSONParser();
		        	try {
		        		response.offer((JSONObject) parser.parse(new String(body, "UTF-8")));
		        	}
		        	catch(Exception e){
		        		e.printStackTrace();
		        	}
		        }
		      }
		    });*/
		}
		catch(ShutdownSignalException e){
			System.out.println("gaya");
		}
		catch(Exception e){
			e.printStackTrace();
			//return null;
		}
		return ret;
	}

	public JSONObject listenAndFilterOnHeader(String field, String value, String mqName){
		/*Used when you have to filter based on some header field in the msg. Side effects: correlation Id and replyMq name will be stored in 
		 * this object only
		 */
		JSONObject ret;
		try{
			/*Get a ConnectionFactory object */
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost(ip);
			factory.setPort(port);
			factory.setUsername(user);
			factory.setPassword(password);
			/* Create a connection */
			Connection connection = factory.newConnection();
			/* Create a channel over that TCP/IP connection */
			Channel channel = connection.createChannel();
			final BlockingQueue<JSONObject> response = new ArrayBlockingQueue<JSONObject>(1);
			Consumer consumer = new DefaultConsumer(channel) {
				@Override
				public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
					Map<String, Object> headers = properties.getHeaders();
					if (headers!=null && headers.containsKey(field) && headers.get(field).toString().equals(value)) {
						JSONParser parser = new JSONParser();
						try {
							channel.basicAck(envelope.getDeliveryTag(), false);
							replyMqName = properties.getReplyTo();
							correlationId = properties.getCorrelationId();
							response.offer((JSONObject) parser.parse(new String(body, "UTF-8")));
						}
						catch(Exception e){
							e.printStackTrace();
						}
					}
					else
						channel.basicNack(envelope.getDeliveryTag(), false, true);
				}
			};

			channel.basicConsume(mqName, false, consumer);
			ret = response.take();
			//if(channel.isOpen()) channel.close();
			if(connection.isOpen()) connection.close();
		}
		catch(Exception e){
			e.printStackTrace();
			return null;
		}
		return ret;

	}

	public JSONObject listenAndFilterOnHeaderAndCorrId(String field, String value, String mqName){
		/*Used when you have to filter based on some header field in the msg AND correlation Id too 
		 */
		JSONObject ret;
		try{
			/*Get a ConnectionFactory object */
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost(ip);
			factory.setPort(port);
			factory.setUsername(user);
			factory.setPassword(password);
			/* Create a connection */
			Connection connection = factory.newConnection();
			/* Create a channel over that TCP/IP connection */
			Channel channel = connection.createChannel();
			final BlockingQueue<JSONObject> response = new ArrayBlockingQueue<JSONObject>(1);
			Consumer consumer = new DefaultConsumer(channel) {
				@Override
				public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
					Map<String, Object> headers = properties.getHeaders();
					if (properties.getCorrelationId().equals(correlationId) && headers!=null && headers.containsKey(field) && headers.get(field).toString().equals(value)) {
						JSONParser parser = new JSONParser();
						try {
							channel.basicAck(envelope.getDeliveryTag(), false);
							response.offer((JSONObject) parser.parse(new String(body, "UTF-8")));
						}
						catch(Exception e){
							e.printStackTrace();
						}
					}
					else
						channel.basicNack(envelope.getDeliveryTag(), false, true);
				}
			};

			channel.basicConsume(mqName, false, consumer);
			ret = response.take();
			//if(channel.isOpen()) channel.close();
			if(connection.isOpen()) connection.close();
		}
		catch(Exception e){
			e.printStackTrace();
			return null;
		}
		return ret;

	}

	public JSONObject listenAndOptionallyFilterOnHeader(String field, String value, String mqName){
		/*Used when you have to filter based on some header field in the msg and 
		 * SIMPLY ignore the header-filtering if there is no header field in the msg 
		 * Side effects: correlation Id and replyMq name will be stored in 
		 * this object only
		 */
		JSONObject ret = null;
		try{
			/*Get a ConnectionFactory object */
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost(ip);
			factory.setPort(port);
			factory.setUsername(user);
			factory.setPassword(password);
			/* Create a connection */
			Connection connection = factory.newConnection();
			/*connection.addShutdownListener(new ShutdownListener() {
			    @Override
			    public void shutdownCompleted(ShutdownSignalException cause) {
			    	//connection = factory.newConnection();
			    	System.out.println("Reason: "+cause.toString());
			    }
			  });
			/* Create a channel over that TCP/IP connection */
			Channel channel = connection.createChannel();
			/*channel.addShutdownListener(new ShutdownListener() {
			    @Override
			    public void shutdownCompleted(ShutdownSignalException cause) {
			    	//channel = connection.createChannel();
			    	listenAndFilterOnHeaderAndCorrId(null, null, null);
			    	System.out.println("Reason: "+cause.toString());
			    }
			  });*/
			final BlockingQueue<JSONObject> response = new ArrayBlockingQueue<JSONObject>(1);
			Consumer consumer = new DefaultConsumer(channel) {
				@Override
				public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
					Map<String, Object> headers = properties.getHeaders();
					/*if(headers!=null){
	            		for(Map.Entry<String, Object> i : headers.entrySet())
		            		System.out.println(i.getKey() + " " + i.getValue().toString());
	            	}*/
					if (headers==null || (headers.containsKey(field) && headers.get(field).toString().equals(value))) {
						JSONParser parser = new JSONParser();
						try {
							channel.basicAck(envelope.getDeliveryTag(), false);
							replyMqName = properties.getReplyTo();
							correlationId = properties.getCorrelationId();
							response.offer((JSONObject) parser.parse(new String(body, "UTF-8")));
						}
						catch(Exception e){
							e.printStackTrace();
						}
					}
					else
						channel.basicNack(envelope.getDeliveryTag(), false, true);
				}
			};

			channel.basicConsume(mqName, false, consumer);
			ret = response.take();
			//if(channel.isOpen()) channel.close();
			if(connection.isOpen()) connection.close();
		}
		catch(Exception e){
			e.printStackTrace();
			return null;
		}
		return ret;
	}

	public JSONObject listenAndConsume(String mqName){
		/*Used when a consumer simply wants to listen and consume a msg from
		 * the specified MQ. Side effects: correlation Id and replyMq name will be stored in 
		 * this object only
		 */
		JSONObject ret = null;
		try{
			/*Get a ConnectionFactory object */
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost(ip);
			factory.setPort(port);
			factory.setUsername(user);
			factory.setPassword(password);
			/* Create a connection */
			Connection connection = factory.newConnection();
			/* Create a channel over that TCP/IP connection */
			Channel channel = connection.createChannel();
			channel.basicQos(1);
			final BlockingQueue<JSONObject> response = new ArrayBlockingQueue<JSONObject>(1);
			channel.basicConsume(mqName, true, new DefaultConsumer(channel) {
				@Override
				public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
					JSONParser parser = new JSONParser();
					try {
						response.offer((JSONObject) parser.parse(new String(body, "UTF-8")));
						replyMqName = properties.getReplyTo();
						correlationId = properties.getCorrelationId();
						//System.out.println("Got msg with corr " + correlationId);
					}
					catch(Exception e){
						e.printStackTrace();
					}
				}
			});
			ret = response.take();
			//if(channel.isOpen()) channel.close();
			if(connection.isOpen()) connection.close();
		}
		catch(Exception e){
			e.printStackTrace();
			return null;
		}
		return ret;
	}

	public int queueDepth(String mqName){
		try{
			/*Get a ConnectionFactory object */
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost(ip);
			factory.setPort(port);
			factory.setUsername(user);
			factory.setPassword(password);
			/* Create a connection */
			Connection connection = factory.newConnection();
			/* Create a channel over that TCP/IP connection */
			Channel channel = connection.createChannel();
			return channel.queueDeclarePassive(mqName).getMessageCount();
		}
		catch(Exception e){
			e.printStackTrace();
			return 0;
		}
	}

	public JSONArray getQueueDetails() {
		JSONArray retArr = null;
		try {
			/* http://127.0.0.1:15672/api/queues */
			URL url = new URL ("http://"+ip+":15672/api/queues");
			byte[] message = "admin:admin".getBytes(StandardCharsets.UTF_8);
			String encoding = Base64.getEncoder().encodeToString(message);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setDoOutput(true);
			connection.setRequestProperty  ("Authorization", "Basic " + encoding);
			InputStream content = (InputStream)connection.getInputStream();
			BufferedReader in = new BufferedReader (new InputStreamReader (content));
			String line;
			String output = "";
			while ((line = in.readLine()) != null)
				output += line;
			JSONParser parser = new JSONParser();
			retArr = (JSONArray) parser.parse(output);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return retArr;
	}

	public HashMap<String, Long> getQueuedepths(){
		JSONArray queues = getQueueDetails();
		HashMap<String, Long> hash = new HashMap<String, Long>();
		try{
			for(int i=0; i<queues.size(); i++)
				hash.put((String)((JSONObject)queues.get(i)).get("name"), (Long)((JSONObject)queues.get(i)).get("messages"));
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return hash;
	}
}