import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.*;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
/**
 * Servlet implementation class ServiceGateway
 */
@WebServlet("/service/*")
public class ServiceGateway extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static int timeOut = 10000; 

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServiceGateway() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		String svcName = request.getRequestURI().substring(request.getRequestURI().lastIndexOf('/')+1);
		JSONObject svcReq = null;
		JSONParser parser = new JSONParser();
		try {
			svcReq = (JSONObject) parser.parse(request.getReader().lines().collect(Collectors.joining(System.lineSeparator())));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		communicateWithMQ mqHandler =  new communicateWithMQ();
		String authToken = null;
		String reqId = UUID.randomUUID().toString();
		if(svcReq.containsKey("command")){
			if(svcReq.get("command").toString().equals("login")){
				if(svcReq.containsKey("usrId") && svcReq.containsKey("passwd")){
					JSONObject authResponse = doAuthentication(svcReq, mqHandler, reqId);
					System.out.println(authResponse.toString());
					if(!authResponse.get("authSuccessful").toString().equals("true")){
						//reply un-authenticated user
						response.getWriter().append("Invalid user");
						return;
					}
					//fully authenticated user - notify authorization service
					authToken =  authResponse.get("token").toString(); 
					notifyAuthorization(svcReq, mqHandler, reqId, authToken);
					response.getWriter().append(authToken);
				}
				else{
					//reply error
					response.getWriter().append("Provide userId and passwd");
					return;
				}
			}
			else if(svcReq.get("command").toString().equals("logout")) {
				System.out.println("Logging out");
				authToken = svcReq.get("authToken").toString();
				doLogout(svcReq, mqHandler, reqId, authToken);
			}
			else{
				//error
				response.getWriter().append("Wrong command argument");
				return;
			}
		}
		else{
			if(!svcReq.containsKey("authToken")){
				if(svcReq.containsKey("usrId") && svcReq.containsKey("passwd")){
					JSONObject authResponse = doAuthentication(svcReq, mqHandler, reqId);
					System.out.println(authResponse.toString());
					if(!authResponse.get("authSuccessful").toString().equals("true")){
						//reply un-authenticated user
						response.getWriter().append("Invalid user");
						return;
					}
					//fully authenticated user - notify authorization service
					authToken =  authResponse.get("token").toString(); 
					notifyAuthorization(svcReq, mqHandler, reqId, authToken);
				}
				else{
					//reply error
					response.getWriter().append("Provide userId and passwd");
					return;
				}
				svcReq.remove("passwd");
			}
			else
				authToken = svcReq.get("authToken").toString();
			String authzationResponse = doAuthorization(svcReq, mqHandler, 
					reqId, authToken, svcName);
			if(!authzationResponse.equals("true")){
				response.getWriter().append("Unauthorized access");
				return;
			}
			svcReq.put("svcName", svcName);
			svcReq.put("timeOut", timeOut);
			svcReq.put("reqId", reqId);
			svcReq.put("stack", "API");
			System.out.println(svcReq.toString());
			mqHandler.send("svcQueueLoadRouter", svcReq, "svcQueueServiceGatewayReply");
			JSONObject reqResponse = mqHandler.listenAndFilter();
			reqResponse.put("authToken", authToken);
			reqResponse.remove("reqId");
			response.getWriter().append(reqResponse.toString());
		}
	}
	
	@SuppressWarnings("unchecked")
	JSONObject doAuthentication(JSONObject svcReq, communicateWithMQ mqHandler, String reqId){
		JSONObject reply = null;
		JSONObject authReq = new JSONObject();
		authReq.put("svcName", "AuthenticationService");
		authReq.put("funcName", "authenticateUser");
		authReq.put("usrId", svcReq.get("usrId").toString());
		authReq.put("reqId", reqId);

		JSONArray params = new JSONArray();
		JSONObject tmp = new JSONObject();
		tmp.put("name", "param1");	//usrId
		tmp.put("type", "String");
		tmp.put("value", svcReq.get("usrId").toString());
		params.add(tmp);
		JSONObject tmp1 = new JSONObject();
		tmp1.put("name", "param2");	//passwd
		tmp1.put("type", "String");
		tmp1.put("value", svcReq.get("passwd").toString());
		params.add(tmp1);
		authReq.put("params", params);
		System.out.println(authReq);
		mqHandler.send("svcQueueAuthenticationService", authReq, "svcQueueServiceGatewayReply");
		JSONParser parser = new JSONParser();
		try {
			reply = (JSONObject) parser.parse(mqHandler.listenAndFilter().get("response").toString());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return reply;
	}
	
	@SuppressWarnings("unchecked")
	String doAuthorization(JSONObject svcReq, communicateWithMQ mqHandler, 
			String reqId, String authToken, String svcName){
		//do Authorization
		JSONObject authorzationReq = new JSONObject();
		authorzationReq.put("svcName", "AuthorizationService");
		authorzationReq.put("funcName", "authorizeUser");
		authorzationReq.put("usrId", svcReq.get("usrId").toString());
		authorzationReq.put("reqId", reqId);
		JSONArray params = new JSONArray();
		JSONObject tmp = new JSONObject();
		tmp.put("name", "param1");	//service
		tmp.put("type", "String");
		tmp.put("value", svcName);
		params.add(tmp);
		JSONObject tmp1 = new JSONObject();
		tmp1.put("name", "param2");	//token
		tmp1.put("type", "String");
		tmp1.put("value", authToken);
		params.add(tmp1);
		authorzationReq.put("params", params);
		System.out.println(authorzationReq.toString());
		mqHandler.send("svcQueueAuthorizationService", authorzationReq, "svcQueueServiceGatewayReply");
		String reply = mqHandler.listenAndFilter().get("response").toString();
		System.out.println(reply);
		return reply;
	}
	
	@SuppressWarnings("unchecked")
	void doLogout(JSONObject svcReq, communicateWithMQ mqHandler, 
		String reqId, String authToken){
		JSONObject logoutReq = new JSONObject();
		logoutReq.put("svcName", "AuthorizationService");
		logoutReq.put("funcName", "deleteTokenEntry");
		logoutReq.put("usrId", svcReq.get("usrId").toString());
		logoutReq.put("reqId", reqId);
		JSONArray params = new JSONArray();
		JSONObject tmp = new JSONObject();
		tmp.put("name", "param1");	//service
		tmp.put("type", "String");
		tmp.put("value", authToken);
		params.add(tmp);
		logoutReq.put("params", params);
		System.out.println(logoutReq.toString());
		mqHandler.send("svcQueueAuthorizationService", logoutReq, "svcQueueServiceGatewayReply");
		mqHandler.listenAndFilter();
	}
	
	@SuppressWarnings("unchecked")
	void notifyAuthorization(JSONObject svcReq, communicateWithMQ mqHandler, String reqId, String authToken){
		JSONObject notifyAuthorization = new JSONObject();
		notifyAuthorization.put("svcName", "AuthorizationService");
		notifyAuthorization.put("funcName", "saveTokenDetails");
		notifyAuthorization.put("usrId", svcReq.get("usrId").toString());
		notifyAuthorization.put("reqId", reqId);		
		JSONArray params = new JSONArray();
		JSONObject tmp = new JSONObject();
		tmp.put("name", "param1");	//token
		tmp.put("type", "String");
		tmp.put("value", authToken);
		params.add(tmp);
		JSONObject tmp1 = new JSONObject();
		tmp1.put("name", "param2");	//usrId
		tmp1.put("type", "String");
		tmp1.put("value", svcReq.get("usrId").toString());
		params.add(tmp1);
		JSONObject tmp2 = new JSONObject();
		tmp2.put("name", "param3");	//timestamp
		tmp2.put("type", "String");
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
		tmp2.put("value", timeStamp);
		params.add(tmp2);
		notifyAuthorization.put("params", params);
		mqHandler.send("svcQueueAuthorizationService", notifyAuthorization, "svcQueueServiceGatewayReply");
		mqHandler.listenAndFilter(); //wait till your request gets serviced off	
	}
}