import org.w3c.dom.*;
import javax.xml.parsers.*;
import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.SimpleDateFormat;

public class AuthorizationService {
	private final static String AUTHORIZED_USER_DETAILS = "authorization_details.xml";
	private static HashMap<String, ArrayList<String>> tokenDetailsMap = new HashMap<String, ArrayList<String>>();
	
	public void saveTokenDetails(String token, String uname, String timeStamp)
	{
		tokenDetailsMap.put(token, new ArrayList<String>());
		tokenDetailsMap.get(token).add(uname);
		tokenDetailsMap.get(token).add(timeStamp); 
		System.out.println("Saved "+tokenDetailsMap.toString());
	}
	
	private void deleteTokenEntry(String token)
	{
		tokenDetailsMap.remove(token);
	}
	
	private void updateTokenEntry(String token,String timeStamp)
	{
		tokenDetailsMap.get(token).add(1,timeStamp);
	}
	
	public boolean authorizeUser(String service, String token) //get uname against token and refresh timeStamp
	{
		System.out.println(service+" "+token);
		System.out.println(tokenDetailsMap.toString());
		try {
			String uname = null;
			String timeStamp = null;
			if(tokenDetailsMap.containsKey(token)){
				uname = tokenDetailsMap.get(token).get(0);
				timeStamp = tokenDetailsMap.get(token).get(1);
				System.out.println(uname);
				String curTimeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
				Timestamp user_ts = Timestamp.valueOf(timeStamp);
				Timestamp cur_ts = Timestamp.valueOf(curTimeStamp);
				long diff = cur_ts.getTime() - user_ts.getTime();
				
				if (diff > 120000){
					deleteTokenEntry(token);
					System.out.println("expired");
					return false;
				}
				else{ 
					String t = cur_ts.toString();
					updateTokenEntry(token,t);
				}
				
				File fXmlFile = new File(AUTHORIZED_USER_DETAILS);
				
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(fXmlFile);
				doc.getDocumentElement().normalize();
				NodeList nList = doc.getElementsByTagName("user");
				
				boolean authResult = false;
				
				for (int temp = 0; temp < nList.getLength(); temp++) {
					Node nNode = nList.item(temp);
					Element eElement = (Element) nNode;
					if(eElement.getAttribute("username").equals(uname)){
						try{
							String result = eElement.getElementsByTagName(service).item(0).getTextContent();
							if(result.equals("true"))
								authResult = true;
							break;
						}catch (Exception e) {
							System.out.println("Unauthorized");
						}
					}
				}
				System.out.println(authResult);
				return authResult;
			}
			else
				return false;
		} catch (Exception e) {
			return false;
		}
	}
}
