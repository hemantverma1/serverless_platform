import javax.naming.*;
import javax.naming.directory.*;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Properties;


public class LdapAdminAuth {


	public static boolean authenticateUser(String ldapadminuname, String ldapadminpassword, String username, String Password) throws Exception{
		try
		{

			String url = "ldap://10.2.128.187:389";
			System.out.println(username+" yahan aaya "+ldapadminpassword);

			Properties props = new Properties();
			props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
			props.put(Context.PROVIDER_URL, url);
			props.put(Context.SECURITY_PRINCIPAL, "cn="+ldapadminuname+",dc=serverless,dc=com");//adminuser - User with special priviledge, dn user
			props.put(Context.SECURITY_CREDENTIALS, ldapadminpassword);//dn user password


			InitialDirContext context = new InitialDirContext(props);
			System.out.println("connected");
			SearchControls ctrls = new SearchControls();
			try {
	            NamingEnumeration<SearchResult> answers = context.search("ou=ias,dc=serverless,dc=com", "(uid=" + username + ")", ctrls);
	            javax.naming.directory.SearchResult result = answers.nextElement();

	            String user = result.getNameInNamespace();
	            props = new Properties();
	            props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
	            props.put(Context.PROVIDER_URL, "ldap://10.2.128.187:389");
	            props.put(Context.SECURITY_PRINCIPAL, user);
	            props.put(Context.SECURITY_CREDENTIALS, Password);

	            context = new InitialDirContext(props);
	        } catch (Exception e) {
	            return false;
	        }
	        
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	public static boolean authenticateJndi(String ldapadminuname, String ldapadminpassword) throws Exception{
		try
		{

			String url = "ldap://10.2.128.187:389";
			System.out.println(ldapadminpassword+" yahan aaya "+ldapadminpassword);

			Properties props = new Properties();
			props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
			props.put(Context.PROVIDER_URL, url);
			props.put(Context.SECURITY_PRINCIPAL, "cn="+ldapadminuname+",dc=serverless,dc=com");//adminuser - User with special priviledge, dn user
			props.put(Context.SECURITY_CREDENTIALS, ldapadminpassword);//dn user password


			InitialDirContext context = new InitialDirContext(props);
			System.out.println("connected");

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	public static void addUser(String ldapadminuname, String ldapadminpassword,String newuser, String newpassword) {  

		try{
			String url = "ldap://10.2.128.187:389";
			Properties props = new Properties();
			props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
			props.put(Context.PROVIDER_URL, url);
			props.put(Context.SECURITY_PRINCIPAL, "cn="+ldapadminuname+",dc=serverless,dc=com");//adminuser - User with special priviledge, dn user
			props.put(Context.SECURITY_CREDENTIALS, ldapadminpassword);//dn user password


			InitialDirContext context = new InitialDirContext(props);
			System.out.println("connected");
			// TODO code application logic here  

			// entry's DN 
			String entryDN = "uid="+newuser+",ou=ias,dc=serverless,dc=com";  

			// entry's attributes  
			String Directory = "/home/"+newuser;
			String login_Shell = "/bin/bash";
			Attribute cn = new BasicAttribute("cn", newuser);  
			Attribute uid = new BasicAttribute("uid",newuser);  
			Attribute uidNumber = new BasicAttribute("uidNumber", "16589");  
			Attribute gidNumber = new BasicAttribute("gidNumber", "100");
			Attribute homeDirectory = new BasicAttribute("homeDirectory", Directory);
			Attribute loginShell = new BasicAttribute("loginShell", login_Shell);
			Attribute gecos = new BasicAttribute("gecos", newuser);
			Attribute userPassword = new BasicAttribute("userPassword", newpassword);
//			Attribute shadowLastChange = new BasicAttribute("shadowLastChange", 0);
//			Attribute shadowMax = new BasicAttribute("shadowMax", 0);
//			Attribute shadowWarning = new BasicAttribute("shadowWarning", 0);
			Attribute oc = new BasicAttribute("objectClass");  
			oc.add("top");  
			oc.add("account");  
			oc.add("posixAccount");  
			oc.add("shadowAccount");  
			DirContext ctx = null;  
			// get a handle to an Initial DirContext  
			ctx = new InitialDirContext(props);  

			// build the entry  
			BasicAttributes entry = new BasicAttributes();  
			entry.put(cn);  
			entry.put(uid);  
			entry.put(uidNumber);  
			entry.put(gidNumber); 
			entry.put(homeDirectory);  
			entry.put(loginShell); 
			entry.put(gecos);  
			entry.put(userPassword); 
//			entry.put(shadowLastChange); 
//			entry.put(shadowWarning); 
//			entry.put(shadowMax); 

			entry.put(oc);  

			// Add the entry  

			ctx.createSubcontext(entryDN, entry);  
			//	          System.out.println( "AddUser: added entry " + entryDN + "."); 
		} catch (NamingException e) {  
			System.err.println("AddUser: error adding entry." + e);  
		}  


	}
	public static ArrayList<String> userList(String ldapadminuname, String ldapadminpassword) {
			Properties props = new Properties();
			props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
			props.put(Context.PROVIDER_URL, "ldap://10.2.128.187:389");
			props.put(Context.SECURITY_PRINCIPAL, "cn="+ldapadminuname+",dc=serverless,dc=com");//adminuser - User with special priviledge, dn user
			props.put(Context.SECURITY_CREDENTIALS, ldapadminpassword);//dn user password

			ArrayList<String> resp = new ArrayList<String>();
			try {
				LdapContext ctx = new InitialLdapContext(props, null);
				ctx.setRequestControls(null);
				NamingEnumeration<?> namingEnum = ctx.search("ou=ias,dc=serverless,dc=com", "(objectclass=posixAccount)", LdapAdminAuth.getSimpleSearchControls());
				while (namingEnum.hasMore ()) {
					SearchResult result = (SearchResult) namingEnum.next ();    
					Attributes attrs = result.getAttributes ();
//					System.out.println(attrs.get("cn"));\
					resp.add(attrs.get("cn").toString().substring(4));
					
				} 
				namingEnum.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return resp;
		}
	private static SearchControls getSimpleSearchControls() {
		SearchControls searchControls = new SearchControls();
		searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		searchControls.setTimeLimit(30000);
		return searchControls;
	}
	public static void deleteUser(String ldapadminuname, String ldapadminpassword,String userID) {

		try {
			String url = "ldap://10.2.128.187:389";
			Properties props = new Properties();
			props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
			props.put(Context.PROVIDER_URL, url);
			props.put(Context.SECURITY_PRINCIPAL, "cn="+ldapadminuname+",dc=serverless,dc=com");//adminuser - User with special priviledge, dn user
			props.put(Context.SECURITY_CREDENTIALS, ldapadminpassword);//dn user password


			InitialDirContext context = new InitialDirContext(props);
		context.destroySubcontext("uid="+userID+",ou=ias,dc=serverless,dc=com");
		context.close();

		} catch (Exception e) {	e.printStackTrace();	}

	}
}
