
public class Try {

	public static void main(String[] args){
		new Thread(new Runnable() {
			public void run() {
				while(true){
					System.out.println("Look ma, no hands");

					try{
						Thread.sleep(2000);
					}catch (InterruptedException e) {
						e.printStackTrace();
					}
				}

			}
		}).start();

		new Thread(new Runnable() {
			public void run() {
				while(true){
					System.out.println("Look at me, look at me...");
					try{
						Thread.sleep(1000);
					}catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();

	}

}
