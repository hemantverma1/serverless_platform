import java.util.*;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import java.io.*;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Log {

		
	String name="LogFile_";
	
		private File getLatestFilefromDir(String dirPath){
		    File dir = new File(dirPath);
		    File[] files = dir.listFiles(new FilenameFilter() { 
                public boolean accept(File dir, String filename)
                { return filename.endsWith(".log"); }
		    	} );
		    
		    if (files == null || files.length == 0) {
		        return null;
		    }

		    File lastModifiedFile = files[0];
		    for (int i = 1; i < files.length; i++) {
		       if ( lastModifiedFile.lastModified() < files[i].lastModified()) {
		           lastModifiedFile = files[i];
		       }
		    }
		    return lastModifiedFile;
		
	}
		
	private String parse(String severity , String uid,String reqid,String processname,String serviceid,String processid,String invokermethod,String log){
		String result="";
		result += new Date() + " ";
		result += severity + " " + uid + " " + reqid + " "+ processname + " "+ serviceid + " "+ processid + " "+ invokermethod + " "+ log;
		return result;

	}
	private void zipFile(File f){
		System.out.println("in zip");
		byte[] buffer = new byte[1024];
		try{

			String file_name = f.getAbsolutePath();
			System.out.println(file_name);
			file_name = file_name.split("\\.")[0]+".zip";
			
			File file = new File(file_name);
			file.createNewFile();
			
			FileOutputStream fos = new FileOutputStream(file);
    		ZipOutputStream zos = new ZipOutputStream(fos);
    		ZipEntry ze= new ZipEntry(file_name);
    		zos.putNextEntry(ze);
    		FileInputStream in = new FileInputStream(f);

    		int len;
    		while ((len = in.read(buffer)) > 0) {
    			zos.write(buffer, 0, len);
    		}

    		in.close();
    		zos.closeEntry();

    		//remember close it
    		zos.close();
    		f.delete();

    		System.out.println("Done");

    	}catch(IOException ex){
    		System.out.println("duf");
    	   ex.printStackTrace();
    	}
	}

	private boolean getFileSize(File f) {
		if(f.length() > 50){
			zipFile(f);
			return true;
		}
		return false;
	}
	
	public void writeLog(String severity,String uid,String reqid,String processname,String serviceid,String processid,String invokermethod,String log) throws IOException{
		File file=getLatestFilefromDir("/home/riddhi/workspace/Logger/Log");
		if(file == null || getFileSize(file)){
			file = new File("/home/riddhi/workspace/Logger/Log/"+name+new Date()+".log");
			file.createNewFile();
		}

		try{
		    FileWriter fw = new FileWriter(file,true); //the true will append the new data
		    fw.write(parse(severity , uid,reqid,processname,serviceid,processid,invokermethod,log));//appends the string to the file
		    fw.close();
		}
		catch(IOException ioe)
		{
		    System.err.println("IOException: " + ioe.getMessage());
		}
	 
		
	}
	
	public static void main(String []args){
		System.out.println("Logger");
		Log l=new Log();
		int i=0;
		try{
		
		l.writeLog("error","user","1234", "prajvocessname","3456","5654","method","log text");
		}
		catch(Exception e){
			
		}
		
	}
}
